package pl.edu.agh.chapter7;

class Wheel {
	private String s;

	Wheel() {
		System.out.println("Wheel()");
		s = "Kolo wyprodukowane";
	}

	public String toString() {
		return s;
	}
}

public class Bike {
	private Wheel w;

	public Bike() {
		System.out.println("Rower");
	}

	public String toString() {
		if (w == null)
			w = new Wheel();
		return "Kolo = " + w;
	}

	public static void main(String[] args) {
		Bike b = new Bike();
		System.out.println(b);
	}
}
