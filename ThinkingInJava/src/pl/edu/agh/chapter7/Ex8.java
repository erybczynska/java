package pl.edu.agh.chapter7;

public class Ex8 {
	private int value; 
	public Ex8(int value) {
		this.setValue(value);
		System.out.println("Konstrukotor klasy bazowej");
	}
	
	public static void main(String[] args) {
		new Ex8a();
		new Ex8a(3);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}

class Ex8a extends Ex8 {
	public Ex8a() {
		super(2);
		System.out.println("Konstrukotor klasy pochodnej domyślny (bezargumentowy)");
	}
	
	public Ex8a(int value) {
		super(value); 
		System.out.println("Konstrukotor klasy pochodnej niedomyślny");
	}
}