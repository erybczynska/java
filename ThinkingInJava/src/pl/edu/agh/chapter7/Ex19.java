package pl.edu.agh.chapter7;

public class Ex19 {
	private int i;
	
	public Ex19(int i) {
		this.i = i;
	}
	
	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}
}

class Ex19a {
	private int j; 
	private final Ex19 e; 
	
	public Ex19a() {
		j = 0; 
		e = new Ex19(2); // blank final musi być zainicjalizowany najpóźniej w konstruktorze
	}
	
	public Ex19a(int j) {
		this.setJ(j); 
		e = new Ex19(1);
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public Ex19 getE() {
		return e;
	}
}
