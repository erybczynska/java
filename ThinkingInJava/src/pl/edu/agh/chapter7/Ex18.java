package pl.edu.agh.chapter7;

public class Ex18 {
	private final static int VAL1 = 1;
	private final int val2;
	
	public Ex18(int val2) {
		this.val2 = val2;
	}

	public static int getVal1() {
		return VAL1;
	}

	public int getVal2() {
		return val2;
	}

	public static void main(String[] args) {
		Ex18 e1 = new Ex18(2);
		System.out.println(e1.getVal2() + " " + Ex18.getVal1()); //statyczne na rzecz klasy
		Ex18 e2 = new Ex18(5);
		System.out.println(e2.getVal2() + " " + Ex18.getVal1());
	}
}
