package pl.edu.agh.chapter7;

public final class Ex22 {
	private int value; 
	
	public Ex22(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}

class Ex22a /*extends Ex22*/ {
	private String s;

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	} 
}