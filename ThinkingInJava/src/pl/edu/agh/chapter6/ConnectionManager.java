package pl.edu.agh.chapter6;

class Connection {
	private Connection() {
		System.out.println("Connection()");
	}

	static Connection makeConnection() {
		return new Connection();
	}
}

public class ConnectionManager {
	static int tabSize = 3;
	static Connection[] tab = new Connection[tabSize];

	public ConnectionManager() {
		for (int i = 0; i < tab.length; i++) {
			tab[i] = Connection.makeConnection();
		}
	}
	
	public static Connection getConnection() {
		if (tabSize > 0) {
			return tab[--tabSize]; 
		}
		else {
			return null;
		}
	}
	
	public static int getTabSize() {
		return tabSize;
	}


	public static void main(String[] args) {
		new ConnectionManager();
		

	}
}
