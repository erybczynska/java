package pl.edu.agh.chapter9;

public class Ex5class implements Ex5 {
	int value;

	@Override
	public void add(int number) {
		value += number;
	}

	@Override
	public int getNumber() {
		return value;
	}

	@Override
	public void printString(String str) {
		System.out.println(str);
	}

}
