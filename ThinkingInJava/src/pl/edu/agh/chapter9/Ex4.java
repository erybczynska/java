package pl.edu.agh.chapter9;

public class Ex4 {

	public static void func(Ex4 e) {
		Ex4a e2 = (Ex4a)e;
		e2.add(3);
		System.out.println(e2.value);
	}

	public static void main(String[] args) {
		Ex4 ex = new Ex4a();
		func(ex);
	}
}

class Ex4a extends Ex4 {
	int value; 
	
	void add(int n) {
		value += n;
	}
}

