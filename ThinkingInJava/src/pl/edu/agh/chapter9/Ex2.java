package pl.edu.agh.chapter9;

abstract class Ex2 {
	int i; 
	public Ex2(int i) {
		this.i = i;
	}
	
	public void showString (String s) {
		System.out.println(s);
	}
	 
	public void add (int number) {
		i =+ number; 
	}
	
	public static void main (String[] args) {
		//Ex2 ex = new Ex2(5); 
	}
}

/* Nie da się utworzyć egzemplarzu klasy, która jest abstakcyjna 
 * a nie ma metod abstrakcyjnych
 */