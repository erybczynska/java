package pl.edu.agh.chapter9;

abstract class Ex3 {
	public abstract void print();

	public Ex3() {
		print();
	}
	
	public static void main(String[] args) {
		Ex3a ex = new Ex3a();
		ex.print();
	}
}

class Ex3a extends Ex3 {
	private int i = 7;

	public void print() {
		System.out.println(i);
	}
}