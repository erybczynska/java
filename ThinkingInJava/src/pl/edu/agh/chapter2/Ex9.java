package pl.edu.agh.chapter2;

public class Ex9 {
	

	public static void main(String[] args) {
		Integer i = 5;
		Float f = 3.4F;
		Character c = 'c';
		Double d = 2.3D;
		Long l = 245606042L;
		Short s = 13;
		Byte b = (byte)1; 
		Boolean bool = true; 
		
		System.out.println(i + " " + f + " " + c + " " + d);
		System.out.println(l + " " + s + " " + b + " " + bool);
	}
}
