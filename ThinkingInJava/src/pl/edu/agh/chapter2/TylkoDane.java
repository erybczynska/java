package pl.edu.agh.chapter2;

public class TylkoDane {
	int i;
	double d; 
	boolean b;
	
	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public double getD() {
		return d;
	}

	public void setD(double d) {
		this.d = d;
	}

	public boolean isB() {
		return b;
	}

	public void setB(boolean b) {
		this.b = b;
	}
	

	public static void main (String[] args) {
		TylkoDane dane = new TylkoDane();
		dane.i = 47;
		dane.d = 1.1;
		dane.b = false;
		
		TylkoDane dane1 = new TylkoDane();
		dane1.setB(true);
		dane1.setI(2);
		dane1.setD(4.5);
		
		System.out.println(dane1.getD());
		System.out.println(dane1.getI());
		System.out.println(dane1.isB());
	}

}
