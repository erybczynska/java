package pl.edu.agh.chapter2;

import java.time.LocalDateTime;

//: object/HelloData.java

/** Pierwszy program przykładowy z 'Thibking in Java'
 * Wyświetla ciąg i dzisieszą datę 
 * @author Bruce Eckel
 * @author www.MindView.net
 * @version 4.0
 */
public class HelloData {
	/** Punkt wejścia do klasy i aplikacji 
	 * @param args tablica ciągów argumentów wywołania
	 * @throws exceptions nie zgłasza wyjatków 
	 */
	public static void main(String[] args) {
		System.out.println("Witaj, dzisiaj jest: ");
		System.out.println(LocalDateTime.now());
	}
} 
