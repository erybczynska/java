package pl.edu.agh.chapter2;

public class WszystkieKoloryTeczy {
	int liczbaCalkowitaKolorow;

	public void zmienOdcienKoloru (int nowyOdcien) {
		liczbaCalkowitaKolorow = nowyOdcien;
	}
	
	public int get () {
		return liczbaCalkowitaKolorow;
	}
	
	public static void main (String[] args) {
		WszystkieKoloryTeczy nowyKolor = new WszystkieKoloryTeczy();
		nowyKolor.liczbaCalkowitaKolorow = 5; 
		nowyKolor.zmienOdcienKoloru(7);
		System.out.println(nowyKolor.get());	
	}
}
