package pl.edu.agh.chapter2;
/**Komentarz odnośnie klasy Tree */
class Tree {
	/** Komentarz dotyczący składowej */
	int height;
	
	/** Komentarz dotyczący metody konstruktor */
	Tree() {
		System.out.println("Sadzenie sadzonki");
		height = 0;
	}
	/** Komentarz dotyczący metody konstruktor parametrowy */
	Tree(int initialHeight) {
		height = initialHeight;
		System.out.println("Tworzenie nowego drzewka, wysokiego na " + height + "metr(y)(ow)");
	}
	/** Komentarz dotyczący metody info */
	void info() {
		System.out.println("Dzewo ma " + height + "metr(y)(ow)");
	}
	/** Komentarz dotyczący metody info z parametrami */
	void info(String s) {
		System.out.println(s + ": drzewo ma " + height + "metr(y)(ow))");
	}
}
/**Komentarz odnośnie klasy Overloading */
public class Overloading {
	/** Komentarz dotyczący metody main*/
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			Tree t = new Tree(i);
			t.info();
			t.info("metody przeciazona");
		}
		new Tree();
	}
}
