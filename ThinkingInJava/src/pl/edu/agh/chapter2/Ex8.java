package pl.edu.agh.chapter2;

public class Ex8 {
	static int x;
	int y; 

	@SuppressWarnings("static-access")
	public static void main (String[] args) {
		Ex8 e1 = new Ex8();
		Ex8 e2 = new Ex8();
		Ex8 e3 = new Ex8();
		
		e1.x++;
		e2.x++;
		e3.x++;
		
		e1.y++;
		e2.y++;
		e3.y++;
		
		System.out.println(e1.x);
		System.out.println(e2.x);
		System.out.println(e3.x);
		System.out.println(e1.y);
		System.out.println(e2.y);
		System.out.println(e3.y);
	}
}
