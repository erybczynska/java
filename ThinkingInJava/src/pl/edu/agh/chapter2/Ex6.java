package pl.edu.agh.chapter2;

public class Ex6 {

	public int wielkosc(String s) {
		return s.length() * 2;
	}

	public static void main(String[] args) {
		Ex6 e = new Ex6();
		System.out.println(e.wielkosc("Lena"));
	}
}
