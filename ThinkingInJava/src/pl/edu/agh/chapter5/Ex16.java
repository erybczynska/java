package pl.edu.agh.chapter5;

public class Ex16 {

	public static void main(String[] args) {
		String[] tabString = new String[3];
		tabString[0] = "Ala";
		tabString[1] = "ma";
		tabString[2] = "kota.";
		
		for (String str : tabString) {
			System.out.println(str);
		}
	} 
}
