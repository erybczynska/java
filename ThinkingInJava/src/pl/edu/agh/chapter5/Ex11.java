package pl.edu.agh.chapter5;

public class Ex11 {
	boolean isOpen = false;

	public Ex11(boolean stateOfHealth) {
		isOpen = stateOfHealth;
	}

	void forgotToLock() {
		isOpen = true;
	}

	void lockTheDoor() {
		isOpen = false;
	}

	protected void finalize() {
		if (isOpen)
			System.out.println("Metoda finalize zostala wykonana");
	}

	public static void main(String[] args) {
		Ex10 home = new Ex10(true);
		home.lockTheDoor();
		System.runFinalization();
		new Ex10(true);
		System.gc();
	}
}