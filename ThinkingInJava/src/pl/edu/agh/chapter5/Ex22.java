package pl.edu.agh.chapter5;

import pl.edu.agh.chapter5.Ex21.Banknotes;

public class Ex22 {
	Banknotes b;

	public static void main(String[] args) {
		for (Banknotes b : Banknotes.values()) {
			System.out.print("Banktot: ");
			switch (b) {
			case TEN:
				System.out.println("10 zl");
				break;
			case TWENTY:
				System.out.println("20 zl");
				break;
			case FIFTY:
				System.out.println("50 zl");
				break;
			case HUNDRED:
				System.out.println("100 zl");
				break;
			default:
				break;
			}
		}
	}
}