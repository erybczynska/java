package pl.edu.agh.chapter5;

public class Ex4 {

	public Ex4() {
		System.out.println("Konstruktor domyslny");
	}

	public Ex4(String str) {
		this();
		System.out.println(str);
	}

	public static void main(String[] args) {
		new Ex4("Konstrukor sparametryzowany");
	}
}