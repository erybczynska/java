package pl.edu.agh.chapter5;

public class Ex8 {

	public void firstMethod() {
		System.out.println("Metoda");
	}
	
	public void secondMethod() {
		firstMethod(); 
		this.firstMethod();
	}
	
	public static void main(String[] args) {
		Ex8 ex = new Ex8();
		ex.firstMethod();
		ex.secondMethod();
	}
}
