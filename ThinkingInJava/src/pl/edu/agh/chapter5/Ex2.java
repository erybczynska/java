package pl.edu.agh.chapter5;

public class Ex2 {
	String str = "Leda";

	public String getStr() {
		return str;
	}

	public class Ex2a {
		String str;

		public Ex2a(String str) {
			this.str = str;
		}

		public String getStr() {
			return str;
		}

	}

	public static void main(String[] args) {
		Ex2 ex1 = new Ex2();
		System.out.println(ex1.getStr());

		Ex2a ex2 = ex1.new Ex2a("Lenka");
		System.out.println(ex2.getStr());

	}

}
