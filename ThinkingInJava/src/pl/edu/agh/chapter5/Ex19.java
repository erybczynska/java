package pl.edu.agh.chapter5;

public class Ex19 {
	static void showStrings(String... args) {
		for(String s : args)
			System.out.print(s + " ");
		System.out.println();
	}
	public static void main(String[] args) {
		showStrings("Ala", "ma", "kota");
		String[] tabStrings = new String[3];
		tabStrings[0] = "Lena";
		tabStrings[1] = "i";
		tabStrings[2] = "Leda";
		showStrings(tabStrings);
		//showStrings(new String[]{"Leda", "i", "Lena"});
	}
}