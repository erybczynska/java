package pl.edu.agh.chapter5;

public class Ex9 {
	String str; 

	public Ex9 () {
		this("argument");
		System.out.println("Pierwszy konstruktor");
	}
	
	public Ex9 (String str) {
		this.str = str; 
		System.out.println("Drugi konstruktor");
	}
	
	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
	
	public static void main(String[] args) {
		Ex9 ex = new Ex9();		
		ex.getStr();
	}
}
