package pl.edu.agh.chapter5;

public class Ex21 {
	public enum Banknotes {
		TEN, TWENTY, FIFTY, HUNDRED 
	}
	
	public static void main(String[] args) {
		for(Banknotes b : Banknotes.values())
			System.out.println(b + ", ordinal " + b.ordinal());	
	}	
}