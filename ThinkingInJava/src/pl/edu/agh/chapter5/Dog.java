package pl.edu.agh.chapter5;

public class Dog {
	
	void bark() {
		System.out.println("Hau hau");
	}

	void bark(int howManyTime) {
		for (int i = 0; i < howManyTime; i++) {
			System.out.println("Hau");
		}
	}

	void bark(String barking){
		System.out.println(barking);
	}
	
	public static void main(String[] args) {
		Dog dog = new Dog();
		dog.bark();
		dog.bark(4);
		dog.bark("Grrrr");
	}
}
