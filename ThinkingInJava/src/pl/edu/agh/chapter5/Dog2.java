package pl.edu.agh.chapter5;

public class Dog2 {
	
	void bark() {
		System.out.println("Hau hau");
	}

	void bark(int howManyTime, String barking) {
		for (int i = 0; i < howManyTime; i++) {
			System.out.println(barking);
		}
	}

	void bark(String barking, int howManyTimes) {
			System.out.println(barking + " " + howManyTimes);
	}

	public static void main(String[] args) {
		Dog2 dog = new Dog2();
		dog.bark();
		dog.bark(4, "Hau");
		dog.bark("Grrrr", 3);
	}
}
