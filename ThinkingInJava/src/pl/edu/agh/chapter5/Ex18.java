package pl.edu.agh.chapter5;

public class Ex18 {

	public Ex18(String str) {
		System.out.println(str);
	}

	public static void main(String[] args) {
		Ex18[] tabEx18 = new Ex18[3];

		tabEx18[0] = new Ex18("Konstruktor");
		tabEx18[1] = new Ex18("Konstruktor");
		tabEx18[2] = new Ex18("Konstruktor");

		for (int i = 0; i < tabEx18.length; i++)
			tabEx18[i] = new Ex18(Integer.toString(i));
	}
}
