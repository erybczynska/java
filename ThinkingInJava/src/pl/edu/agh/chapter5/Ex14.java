package pl.edu.agh.chapter5;

public class Ex14 {
	static String str1 = "String nr.1"; 
	static String str2; 
	
	static {
		str2 = "String nr.2";
		System.out.println("Inicjalizacja drugiego Stringa");
	}
	
	public static void getStrings() {
		System.out.println(str1 + str2);
	}
	
	public static void main(String[] args) {
		getStrings();
	}
}
