package pl.edu.agh.chapter3;

public class Ex13 {

	public static String charToBin(char c) {
		return Integer.toBinaryString((int) c);
	}

	public static void main(String[] args) {
		char c1 = 'g';
		char c2 = '1';
		char c3 = 'A';
		System.out.println(charToBin(c1));
		System.out.println(charToBin(c2));
		System.out.println(charToBin(c3));				
	}
}
