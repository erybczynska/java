package pl.edu.agh.chapter3;

public class Dog2 {
	String name;
	String says;

	public Dog2(String name, String says) {
		this.name = name;
		this.says = says;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSays() {
		return says;
	}

	public void setSays(String says) {
		this.says = says;
	}

	public static void main(String[] str) {
		Dog2 spot = new Dog2("Spot", "Hau");
		Dog2 scruffy = new Dog2("Scruffy", "Wrrr");
		Dog2 dog = spot;
		
		System.out.println(dog == spot);
		System.out.println(dog == scruffy);
		System.out.println(spot == scruffy);
		
		System.out.println(dog.equals(spot));
		System.out.println(dog.equals(scruffy));
		System.out.println(spot.equals(scruffy));
	}
}
