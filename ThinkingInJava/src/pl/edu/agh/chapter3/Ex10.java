package pl.edu.agh.chapter3;

public class Ex10 {
	final static int x1 = 0b1010101010;
	final static int x2 = 0b10101010101;
	
	
	public static void main(String[] args) {
		System.out.println(Integer.toBinaryString(x1 & x2));
		System.out.println(Integer.toBinaryString(x1 | x2));
		System.out.println(Integer.toBinaryString(x1 ^ x2));
		System.out.println(Integer.toBinaryString(~x1));
		System.out.println(Integer.toBinaryString(~x2));
	}
}
