package pl.edu.agh.chapter3;

public class Ex11 {

	public static int move(int number, int howManyTimes) {
		if (howManyTimes > 0)
			return number >> howManyTimes;
		else
			return number << -howManyTimes;
	}

	public static void main(String[] args) {
		int x = 0b1010101110;
		System.out.println(Integer.toBinaryString(x));
		System.out.println(Integer.toBinaryString(move(x, 2)));
		System.out.println();
		System.out.println(Integer.toBinaryString(x));
		System.out.println(Integer.toBinaryString(move(x, -2)));
	}
}
