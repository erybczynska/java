package pl.edu.agh.chapter3;

public class Ex4 {
	private double distance; 
	private double time; 

	public double getSpeed() {
		return (distance/time);
	}
	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}
	
	public static void main (String[] args) {
		Ex4 ex = new Ex4();
	    ex.setDistance(12.0);
	    ex.setTime(2.5); 
	    System.out.println(ex.getSpeed());		
	}
}
