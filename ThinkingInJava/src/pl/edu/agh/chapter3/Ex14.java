package pl.edu.agh.chapter3;

public class Ex14 {

	public static void getComparison(String s1, String s2) {
		if (s1 == s2)
			System.out.println("Stringi sa takie same");
		else 
			System.out.println("Stringi sa rozne");
		
		if (s1 != s2)
			System.out.println("Stringi sa rozne");
		else 
			System.out.println("Stringi sa takie same");
		
		if (s1.equals(s2))
			System.out.println("Stringi sa takie same");
		else 
			System.out.println("Stringi sa rozne");
	}
	
	public static void main(String[] args) {
		getComparison("Leda", "Lena");
	}
}
