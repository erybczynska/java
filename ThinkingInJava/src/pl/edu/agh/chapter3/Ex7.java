package pl.edu.agh.chapter3;

import java.util.Random;

public class Ex7 {

	public static int[] draw(int howManyTimes) {
		Random generator = new Random();
		int[] results = new int[howManyTimes];
		for (int i = 0; i < howManyTimes; i++) {
			results[i] = generator.nextInt(2);
		}
		return results;
	}

	public static void main(String[] str) {
		int[] tab = draw(5);
		for (int x : tab) {
			if (x == 0)
				System.out.println("Resztka");
			else
				System.out.println("Orzel");
		}
	}
}
