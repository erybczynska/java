package pl.edu.agh.chapter3;

public class Dog {
	String name;
	String says;

	public Dog(String name, String says) {
		this.name = name;
		this.says = says;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSays() {
		return says;
	}

	public void setSays(String says) {
		this.says = says;
	}

	public static void main(String[] str) {
		Dog spot = new Dog("Spot", "Hau");
		Dog scruffy = new Dog("Scruffy", "Wrrr");
		
		System.out.println(spot.getName() + " " + spot.getSays());
		System.out.println(scruffy.getName() + " " + scruffy.getSays());		
	}
}
