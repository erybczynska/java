package pl.edu.agh.chapter3;

public class Ex12 {

	public static int move(int number, int howManyTimes) {
		if (howManyTimes > 0)
			return number >>> howManyTimes;
		else
			return number << -howManyTimes;
	}

	public static void main(String[] args) {
		int x = 0b11111111111111111111111111111111;
		System.out.println(x);
		System.out.println(Integer.toBinaryString(x));
		System.out.println(Integer.toBinaryString(move(x, -1)));
		x = move(x, -1);
		System.out.println();
		
	
		System.out.println(Integer.toBinaryString(x));
		System.out.println(Integer.toBinaryString(move(x, 3)));
		x = move(x, 3);
		System.out.println(x);
	}
}
