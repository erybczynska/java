package pl.edu.agh.chapter3;

public class Ex8 {

	public static void main(String[] args) {
		long x1 = 0100_000_000_000L;
		long x2 = 0x100_000_000_000L;

		System.out.println(Long.toBinaryString(x1));
		System.out.println(Long.toBinaryString(x2));
	}
}
