package pl.edu.agh.chapter3;

public class Ex3 {
	private float data;

	public Ex3(float data) {
		this.data = data;
	}

	public float getData() {
		return data;
	}

	public void setData(float data) {
		this.data = data;
	}
	
	public static void changeData (Ex3 e) {
		e.setData(5.4F);
	}

	public static void main(String[] str) {
		Ex3 e1 = new Ex3(2.3F);
		Ex3 e2 = e1;
		
		System.out.println(e1.getData());
		System.out.println(e2.getData());
		System.out.println();
		
		changeData(e1);
		changeData(e2);
		
		System.out.println(e1.getData());
		System.out.println(e2.getData());
		System.out.println();
	}
}
