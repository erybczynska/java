package pl.edu.agh.chapter3;

public class Ex2 {
	private float data;

	public Ex2(float data) {
		this.data = data;
	}

	public float getData() {
		return data;
	}

	public void setData(float data) {
		this.data = data;
	}

	public static void main(String[] str) {
		Ex2 e1 = new Ex2(2.3F);
		Ex2 e2 = e1;

		System.out.println(e1.getData());
		System.out.println(e2.getData());
		System.out.println();

		e1.setData(3.4F);
		System.out.println(e1.getData());
		System.out.println(e2.getData());
		System.out.println();

		e2.setData(5.9F);
		System.out.println(e1.getData());
		System.out.println(e2.getData());
	}
}
