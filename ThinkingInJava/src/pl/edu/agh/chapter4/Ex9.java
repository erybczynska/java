package pl.edu.agh.chapter4;

public class Ex9 {

	static void getFib(int howMany) {
		if (howMany < 1) {
			return;
		}
		if (howMany == 1) {
			System.out.println(1);
			return;
		}
		int[] tabFib = new int[howMany];
		
		tabFib[0] = 1;
		tabFib[1] = 1; 
		for(int i = 2; i < howMany; i++) {
			tabFib[i] = tabFib[i-2] + tabFib[i-1];
		}
		
		for (int j = 0; j < howMany; j++) {
			System.out.println(tabFib[j]);
		}	
	}

	public static void main(String[] args) {
		getFib(1);
	}
}
