package pl.edu.agh.chapter4;

public class Ex6 {

	static boolean test(int begin, int end, int testval) {
		if ((testval >= begin) && (testval <= end)) {
			return true; 
		}
		else 
			return false; 
	}

	public static void main(String[] args) {
		if (test(3,10, 7)) 
			System.out.println("Liczba zawiera sie w przedziale");
		else 
			System.out.println("Liczba nie zawiera sie w przedziale");
	}
}
