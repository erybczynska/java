package pl.edu.agh.chapter4;

public class Ex10 {
	static int a(int i) {
		return i / 1000;
	}

	static int b(int i) {
		return (i % 1000) / 100;
	}

	static int c(int i) {
		return (i % 100) / 10;
	}

	static int d(int i) {
		return i % 10;
	}

	static int makeNumber(int i, int j) {
		return (i * 10) + j;
	}

	static boolean isVamipireNumber(int i, int m, int n) {
		if (m * n == i) {
			System.out.println(i);
			return true; 
		}
		return false;
	}
	
	static void printVampiresNumbers() {
		for(int i = 1001; i < 9999; i++) {	
			if(!isVamipireNumber(i, makeNumber(a(i), b(i)), makeNumber(c(i), d(i))))
				if(!isVamipireNumber(i, makeNumber(a(i), b(i)), makeNumber(d(i), c(i))))
					if(!isVamipireNumber(i, makeNumber(a(i), c(i)), makeNumber(b(i), d(i))))
						if(!isVamipireNumber(i, makeNumber(a(i), c(i)), makeNumber(d(i), b(i))))
							if(!isVamipireNumber(i, makeNumber(a(i), d(i)), makeNumber(b(i), c(i))))
								if(!isVamipireNumber(i, makeNumber(a(i), d(i)), makeNumber(c(i), b(i))))
									if(!isVamipireNumber(i, makeNumber(b(i), a(i)), makeNumber(c(i), d(i))))
										if(!isVamipireNumber(i, makeNumber(b(i), a(i)), makeNumber(d(i), c(i))))
											if(!isVamipireNumber(i, makeNumber(b(i), c(i)), makeNumber(d(i), a(i))))
												if(!isVamipireNumber(i, makeNumber(b(i), d(i)), makeNumber(c(i), a(i))))
													if(!isVamipireNumber(i, makeNumber(c(i), a(i)), makeNumber(d(i), b(i))))
														isVamipireNumber(i, makeNumber(c(i), b(i)), makeNumber(d(i), a(i)));
		}	
	}

	public static void main(String[] args) {
		printVampiresNumbers();
	}
}