package pl.edu.agh.chapter4;

import java.util.Random;

public class Ex2 {

	static void getComparison(int firstNumber, int secondNumber) {
		if (firstNumber > secondNumber) {
			System.out.println("Pierwsza liczba wieksza od drugiej.");
		}
		else if (firstNumber < secondNumber) {
			System.out.println("Pierwsza liczba mniejsza od drugiej.");
		} else {
			System.out.println("Liczby sa rowne.");
		}
	}

	public static void main(String[] args) {
		int a,b;
		Random generator = new Random();
		for (int i = 0; i < 25; i++) {
			a = generator.nextInt(100);
			b = generator.nextInt(100);
			System.out.println((i + 1) + ". " + a + " " + b);
			getComparison(a, b);
		}
	}
}
