package pl.edu.agh.chapter4;

public class Ex4 {

	static void showPrimeNumbers(int max) {
		boolean[] tab = new boolean[max + 1];

		for (int i = 2; i * i <= max; i++) {
			if (!tab[i]) {
				for (int j = 2 * i; j <= max; j += i) {
					tab[j] = true;
				}
			}

		}

		for (int i = 2; i <= max; i++) {
			if (!tab[i]) {
				System.out.print(i + " ");
			}
		}

	}

	public static void main(String[] args) {
		showPrimeNumbers(30);
	}
}
