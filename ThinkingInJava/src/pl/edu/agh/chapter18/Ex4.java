package pl.edu.agh.chapter18;

import java.io.File;

import pl.edu.agh.chapter18.Directory.TreeInfo;

public class Ex4 {
	public static void main(String[] args) {
		File testFile = new File("."); 
		TreeInfo list = Directory.walk(testFile, "[^i]*i.*");
		long sum = 0; 
		for (File x : list) {
			System.out.println(x.getName() + " " + x.length() + " bajtów");
			sum += x.length();
		}
		System.out.println();
		System.out.println("Suma - " + sum/(1024) + " kB");
	}
}
