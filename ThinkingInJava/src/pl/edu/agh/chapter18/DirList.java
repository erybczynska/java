package pl.edu.agh.chapter18;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

public class DirList {
	public static void main(String[] args) {
		File path = new File(".");
		String[] list;
		if (args.length == 0)
			list = path.list();
		else
			list = path.list(new DirFilter(args));
		Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);
		for (String dirItem : list)
			System.out.println(dirItem);
	}
}

class DirFilter implements FilenameFilter {
	private String[] wordsToFind;

	public DirFilter(String[] wordsToFind) {
		this.wordsToFind = wordsToFind;
	}

	public boolean accept(File dir, String name) {
		File file = new File(dir, name);
		if (!file.isFile())
			return false;

		String fileData = TextFile.read(name);
		for (String str : wordsToFind) {
			if (fileData.contains(str))
				return true;
		}
		return false;
	}
}