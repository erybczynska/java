package pl.edu.agh.chapter18;

import java.io.File;
import java.util.Arrays;

public class SortedDirList {
	private String[] list;

	public SortedDirList(File filePath) {
		list = filePath.list();
		Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);
	}

	public void list() {
		for (String str : list) {
			System.out.println(str);
		}
	}

	public void list(String regex) {
		for (String str : list) {
			if (str.matches(regex))
				System.out.println(str);
		}
	}

	public static void main(String[] args) {
		File testFile = new File(".");
		SortedDirList test = new SortedDirList(testFile);
		test.list();
		System.out.println();
		test.list("[^i]*i.*");
	}
}
