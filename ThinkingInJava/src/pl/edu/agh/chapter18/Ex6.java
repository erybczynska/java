package pl.edu.agh.chapter18;

import java.io.File;
import java.util.Date;

public class Ex6 {
	public static void main(String[] args) {
		ProcessFiles test = new ProcessFiles(new ProcessFiles.Strategy() {
			public void process(File file) {
				if (file.lastModified() > (System.currentTimeMillis() - (1000 * 60 * 60 * 24))) {
					Date d = new Date(file.lastModified());
					System.out.println(file.getName() + " " + d);
				}
			};
		}, "java");
		test.start(args);
	}
}
