package pl.edu.agh.chapter18;

import java.util.regex.*;
import java.io.*;
import java.util.*;

public class DirList3 {
	public static void main(final String[] args) {
		File path = new File(".");
		String[] list;
		if (args.length == 0)
			list = path.list();
		else
			list = path.list(new FilenameFilter() {
				private Pattern pattern = Pattern.compile(args[0]);

				public boolean accept(File dir, String name) {
					
					return pattern.matcher(name).matches();
				}
			});
		Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);
		long sum = 0; 
		for (String dirItem : list) {
			System.out.print(dirItem);
			File file = new File(".", dirItem);
			System.out.println(" " + file.length() + " bajtów");
			sum = sum + file.length();
		}
		System.out.println("Suma rozmiarów plików - " + sum/(1024) + " kB");
		
	}
}
