package pl.edu.agh.chapter18;

import java.io.*;

public class ProcessFiles2 {
	public interface Strategy {
		void process(File file);
	}

	private Strategy strategy;
	private String regex;

	public ProcessFiles2(Strategy strategy, String regex) {
		this.strategy = strategy;
		this.regex = regex;
	}

	public void start(String[] args) {
		try {
			if (args.length == 0)
				processDirectoryTree(new File("."));
			else
				for (String arg : args) {
					File fileArg = new File(arg);
					if (fileArg.isDirectory())
						processDirectoryTree(fileArg);
					else {
						strategy.process(new File(arg).getCanonicalFile());
					}
				}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void processDirectoryTree(File root) throws IOException {
		for (File file : Directory.walk(root.getAbsolutePath(), regex))
			strategy.process(file.getCanonicalFile());
	}

	public static void main(String[] args) {
		new ProcessFiles2(new ProcessFiles2.Strategy() {
			public void process(File file) {
				System.out.println(file);
			}
		}, "D.*").start(args);
	}
}